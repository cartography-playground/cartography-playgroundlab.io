//==============================================================================
//  GENERAL STUFF
//==============================================================================

// primary color from global css
var color_primary = getComputedStyle(document.body).getPropertyValue("--primary");
var btn_clear = document.getElementById("cl2p-clear");
var chartctx = document.getElementById("chart");
var profile_samples = 128; //FIXME: open elevation crashes whit too much samples, google and bing are ok with 256
var profile_length = -1;

//==============================================================================
//  CHART
//==============================================================================

var profile_chart = new Chart(chartctx, {
  type: "line",
  data: {
    labels: Array(profile_samples).fill(""),
    datasets: [{
      data: Array(profile_samples).fill(0),
      backgroundColor: hex2rgb(color_primary, 0.5),
      borderColor: color_primary,
      pointRadius: 0,
      fill: "origin"
    }]
  },
  options: {
    legend: {
      display: false
    },
    tooltips: {
      enabled: false
    },
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Elevation [meter]"
        }
      }],
      xAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: "Length of path [meter]"
        },
        ticks: {
          maxRotation: 0,
          autoSkip: true,
          maxTicksLimit: 10
        }
      }]
    },
    maintainAspectRatio: false
  }
});

//==============================================================================
//  MAP
//==============================================================================

// Init map
mapboxgl.accessToken = mapbox_accessToken;
var map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/outdoors-v9",
  center: [11.372654, 47.625774], // starting position [lng, lat]
  zoom: 13, // starting zoom
  pitchWithRotate: false,
  keyboard: false
});

// add map controlls
map.addControl(new mapboxgl.ScaleControl(), "bottom-right");
map.addControl(new mapboxgl.NavigationControl(), "bottom-right");
map.addControl(new MapboxGeocoder({ accessToken: mapboxgl.accessToken }), "top-right");

// GeoJSON object to hold our measurement features
var geojson = {
  "type": "FeatureCollection",
  "features": []
};

// Used to draw a line between points
var linestring = {
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": []
  }
};

map.on("load", function () {

  // Hide unnecessary layers from outdoors style
  var hide_layers = ["housenum-label", "poi-relevant-scalerank4-l15", "poi-relevant-scalerank4-l1", "poi-parks_scalerank4", "poi-scalerank3", "poi-parks-scalerank3", "road-shields-black", "road-shields-white", "motorway-junction", "poi-outdoor-features"];
  hide_layers.forEach(function (layer) {
    map.setLayoutProperty(layer, "visibility", "none");
  });

  // Emphasise contours
  map.setPaintProperty("contour-line", "line-color", "#404040");
  map.setPaintProperty("contour-line", "line-opacity", { "base": 1, "stops": [[11, 0.2],[12, 0.4]] });
  map.setPaintProperty("contour-line", "line-width", { "base": 1, "stops": [[13, 0.6], [16, 1.0]] });
  map.setPaintProperty("contour-line-index", "line-color", "#000000");
  map.setPaintProperty("contour-line-index", "line-opacity", { "base": 1, "stops": [[11, 0.3],[12, 0.6]] });
  map.setPaintProperty("contour-line-index", "line-width", { "base": 1, "stops": [[13, 0.8], [16, 1.2]] });
  map.setPaintProperty("contour-label", "text-color", "#000000");

  // Add source for clicked line
  map.addSource("geojson", {
    "type": "geojson",
    "data": geojson
  });

  // Add styles to the map
  map.addLayer({
    id: "measure-points",
    type: "circle",
    source: "geojson",
    paint: {
      "circle-radius": 4,
      "circle-color": "#ffffff",
      "circle-stroke-width": 4,
      "circle-stroke-color": color_primary
    },
    filter: ["in", "$type", "Point"]
  });
  map.addLayer({
    id: "measure-lines",
    type: "line",
    source: "geojson",
    layout: {
      "line-cap": "round",
      "line-join": "round"
    },
    paint: {
      "line-color": color_primary,
      "line-width": 2.5
    },
    filter: ["in", "$type", "LineString"]
  }, "measure-points"); // add "below" this layer


  map.on("click", function (e) {
    var features = map.queryRenderedFeatures(e.point, { layers: ["measure-points"] });

    // Remove the linestring from the group
    // So we can redraw it based on the points collection
    if (geojson.features.length > 1) geojson.features.pop();

    // If a feature was clicked, remove it from the map
    if (features.length) {
      var id = features[0].properties.id;
      geojson.features = geojson.features.filter(function (point) {
        return point.properties.id !== id;
      });
      if (geojson.features.length < 2) {
        // clear profile plot
        update_profile(null, null);
        // clear length
        profile_length = -1;
      }
    } else {
      // Else add point
      var point = {
        "type": "Feature",
        "geometry": {
          "type": "Point",
          "coordinates": [e.lngLat.lng, e.lngLat.lat]
        },
        "properties": {
          "id": String(new Date().getTime())
        }
      };

      geojson.features.push(point);
    }

    if (geojson.features.length > 1) {
      linestring.geometry.coordinates = geojson.features.map(function (point) {
        return point.geometry.coordinates;
      });

      geojson.features.push(linestring);

      // total distance
      profile_length = turf.lineDistance(linestring);

      // get elevation and plot it
      elevation_request_plot(linestring, profile_length);
    }

    map.getSource("geojson").setData(geojson);
  }); // map on click

  map.on("mousemove", function (e) {
    var features = map.queryRenderedFeatures(e.point, { layers: ["measure-points"] });
    // UI indicator for clicking/hovering a point on the map
    map.getCanvas().style.cursor = features.length ? "pointer" : "crosshair";
  }); // map on mousemove
}); // map on load


//==============================================================================
//  Elevation
//==============================================================================

var select_ele = document.getElementById("select_elevation");
select_ele.addEventListener("change", function () {elevation_request_plot(linestring, profile_length);}, false);
var labels_array = [];
var data_array = [];

function elevation_request_plot(linestring, length) {

  if (length == -1) {return;} // workaround for selecting API before drawing linestring

  data_array = [];
  labels_array = [];
  var length_increment = length / profile_samples * 1000; // in meters

  for (i = 0; i < profile_samples; i++) {
    var cur_length = Math.ceil(i * length_increment / 10) * 10; // rounded to tens
    labels_array.push(cur_length);
  }

  $(document).ajaxStart(function() { $(elevation_loading).css("visibility", "visible"); });
  $(document).ajaxStop(function() { $(elevation_loading).css("visibility", "hidden"); });

  switch (select_ele.value) {

    case "Google Maps":

      var path = [];
      linestring.geometry.coordinates.forEach(function (c) {
        path.push({ lat: c[1], lng: c[0] });
      });

      var elevator = new google.maps.ElevationService();
      elevator.getElevationAlongPath({
        "path": path,
        "samples": profile_samples
      }, function(elevations, status) {
        if (status !== "OK") {
          alert("⚠️🚫🗺️📈\nCannot show elevation: request failed because\n" + status);
          return;
        }
        for (i = 0; i < profile_samples; i++) {
          data_array.push(elevations[i].elevation);
        }

        update_profile(labels_array, data_array);
      });

      break;

    case "Bing Maps":

      var bingmaps_data = "";
      linestring.geometry.coordinates.forEach(function (coord) {
        bingmaps_data += coord[1] + "," + coord[0] + ",";
      });
      bingmaps_data = bingmaps_data.slice(0, -1);

      $.ajax({
        url: "https://dev.virtualearth.net/REST/v1/Elevation/Polyline?points=" + bingmaps_data + "&samples=" + profile_samples + "&key=" + bingsmaps_key,
        method: "GET"
      })
      .done(function(res) {
        var data_array = res.resourceSets[0].resources[0].elevations;
        update_profile(labels_array, data_array);
      })
      .fail(function(err) {
        console.log(err.responseJSON);
        alert("⚠️🚫🗺️📈\nCannot show elevation: request failed because\n" + err.statusText);
      });

      break;

    case "Open-Elevation":

      var openelevation_data = {"locations":[]};
      for (i = 0; i < profile_samples; i++) {
        var point = turf.along(linestring, i*length/profile_samples).geometry.coordinates;
        openelevation_data.locations.push({"latitude": point[1], "longitude": point[0]});
      }

      $.ajax({
        url: "https://api.open-elevation.com/api/v1/lookup",
        method: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(openelevation_data)
      })
      .done(function(json) {
        json.results.map(function(res) {data_array.push(res.elevation);});
        update_profile(labels_array, data_array);
      })
      .fail(function(err) {
        console.log(err);
        alert("⚠️🚫🗺️📈\nCannot show elevation: request failed because\n" + err.statusText);
      });

    }//switch

}

function update_profile(labels, data) {
  if (labels == null) {
    labels = Array(profile_samples).fill("");
  }
  if (data == null) {
    data = Array(profile_samples).fill(0);
  }

  // update profile plot
  profile_chart.data.labels.pop();
  profile_chart.data.labels = labels;
  profile_chart.data.datasets[0].data.pop();
  profile_chart.data.datasets[0].data = data;

  profile_chart.update();
}

function clear_data() {
  profile_chart.config.data.datasets.splice(0, 1);
  myLine.update();
}

//==============================================================================
//  GENERAL STUFF
//==============================================================================

function clear() {
  // clear map
  geojson.features = [];
  linestring.geometry.coordinates = [];
  map.getSource("geojson").setData(geojson);
  // clear profile plot
  update_profile(null, null);
  // clear profile_length
  profile_length = -1;
}

function hex2rgb(hex, alpha) {
  var hex_val = hex.slice(-6);
  var bigint = parseInt(hex_val, 16);
  var r = bigint >> 16 & 255;
  var g = bigint >> 8 & 255;
  var b = bigint & 255;

  if (alpha) {
    return "rgba(" + r + "," + g + "," + b + "," + alpha + ")";
  } else {
    return "rgb(" + r + "," + g + "," + b + ")";
  }
}

btn_clear.addEventListener("click", function (e) {clear();});
