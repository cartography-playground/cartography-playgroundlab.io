//==============================================================================
//  General
//==============================================================================

var size_x = 500;
var size_y = 300;
var marker_size = 6;
var flag = false;

var valuemin = 0, valuemax = 0; // values for kernel density function

var draw = SVG("drawing");
draw.viewbox(0, 0, size_x, size_y);

var dataPoints = [];
var centroids = [];
var dataPointsAssigned = {};
var addingDataPointsManually = false;
var runningKDE = false;
var timeout_kde = 500;
var eps_drawing = draw.group();


var check_draw_points = document.getElementById("check-draw-points");
var btn_data_random = document.getElementById("btn-data-random");
var btn_data_random_clusters = document.getElementById("btn-data-random-clusters");
var btn_data_clusters = document.getElementById("btn-data-clusters");
var btn_data_smiley = document.getElementById("btn-data-smiley");
var btn_data_grid = document.getElementById("btn-data-grid");
var btn_data_clear = document.getElementById("btn-data-clear");

var timer = document.getElementById("timer");

var sel_kfunc = document.getElementById("sel-kfunc");

var btn_kde_run_stop = document.getElementById("btn-kde-run-stop");

var rg_eps = document.getElementById("rg-eps");
var label_eps = document.getElementById("label-eps");


//==============================================================================
//  Setup
//==============================================================================

draw.node.addEventListener("click", function (e) {addNewPoint(getPointClickedOn(e));}, false);

check_draw_points.addEventListener("change", function () {toggleAddingDataPointsManually();}, false);
btn_data_random.addEventListener("click", function () {addDataPointsRandomly(150);}, false);
btn_data_random_clusters.addEventListener("click", function () {addDataPointsRandomClusters();}, false);
btn_data_clusters.addEventListener("click", function () {addDatasetPoints(data_clusters3);}, false);
btn_data_smiley.addEventListener("click", function () {addDatasetPoints(data_smiley);}, false);
btn_data_grid.addEventListener("click", function () {addDatasetPoints(data_grid);}, false);
btn_data_clear.addEventListener("click", function () {removeAllDataPoints();}, false);

btn_kde_run_stop.addEventListener("click", function () {toggleKDE();}, false);

rg_eps.addEventListener("input", function () {label_eps.innerHTML = this.value;epsChange();}, false);
rg_eps.addEventListener("change", function () {epsRemove();}, false);


//==============================================================================
//  Data
//==============================================================================

var data_clusters3 = [{x:72,y:43}, {x:62,y:61}, {x:67,y:76}, {x:82,y:81}, {x:101,y:67}, {x:104,y:52}, {x:91,y:37}, {x:64,y:41}, {x:39,y:63}, {x:37,y:94}, {x:52,y:113}, {x:76,y:119}, {x:104,y:117}, {x:138,y:96}, {x:138,y:59}, {x:122,y:42}, {x:97,y:36}, {x:73,y:38}, {x:57,y:109}, {x:250,y:211}, {x:218,y:216}, {x:203,y:238}, {x:221,y:265}, {x:238,y:275}, {x:255,y:278}, {x:280,y:271}, {x:308,y:240}, {x:302,y:220}, {x:270,y:202}, {x:238,y:201}, {x:215,y:215}, {x:206,y:240}, {x:241,y:265}, {x:265,y:257}, {x:279,y:242}, {x:280,y:225}, {x:243,y:232}, {x:233,y:244}, {x:410,y:41}, {x:361,y:63}, {x:368,y:108}, {x:397,y:131}, {x:424,y:127}, {x:450,y:85}, {x:441,y:56}, {x:416,y:36}, {x:359,y:41}, {x:347,y:73}, {x:391,y:118}, {x:426,y:99}, {x:426,y:80}, {x:405,y:68}, {x:387,y:90}, {x:371,y:68}, {x:375,y:39}, {x:383,y:31}, {x:396,y:54}, {x:414,y:56}, {x:424,y:57}, {x:438,y:35}, {x:454,y:53}, {x:471,y:86}, {x:461,y:110}, {x:463,y:133}, {x:438,y:133}, {x:431,y:139}, {x:404,y:143}, {x:384,y:132}, {x:384,y:127}, {x:366,y:121}, {x:354,y:100}, {x:370,y:87}, {x:391,y:110}, {x:421,y:112}, {x:409,y:96}, {x:412,y:84}, {x:457,y:68}, {x:448,y:114}, {x:394,y:77}, {x:408,y:29}, {x:421,y:23}, {x:457,y:34}, {x:453,y:143}, {x:475,y:119}, {x:410,y:113}, {x:436,y:92}, {x:440,y:70}, {x:423,y:72}, {x:413,y:76}, {x:399,y:86}, {x:402,y:103}, {x:433,y:114}, {x:452,y:102}, {x:391,y:68}, {x:386,y:54}, {x:391,y:39}, {x:402,y:38}, {x:376,y:58}, {x:386,y:70}, {x:391,y:82}, {x:381,y:102}, {x:379,y:115}, {x:359,y:88}, {x:367,y:75}, {x:380,y:75}, {x:411,y:48}, {x:434,y:46}, {x:86,y:60}, {x:79,y:70}, {x:79,y:53}, {x:86,y:46}, {x:91,y:51}, {x:67,y:53}, {x:59,y:53}, {x:53,y:61}, {x:51,y:80}, {x:50,y:93}, {x:66,y:100}, {x:74,y:107}, {x:83,y:108}, {x:97,y:104}, {x:110,y:103}, {x:129,y:83}, {x:138,y:63}, {x:122,y:51}, {x:117,y:73}, {x:114,y:91}, {x:92,y:96}, {x:80,y:105}, {x:62,y:91}, {x:45,y:83}, {x:78,y:85}, {x:97,y:83}, {x:101,y:74}, {x:118,y:61}, {x:101,y:86}, {x:91,y:70}, {x:107,y:44}, {x:258,y:197}, {x:229,y:206}, {x:228,y:227}, {x:219,y:235}, {x:205,y:254}, {x:226,y:276}, {x:238,y:291}, {x:283,y:288}, {x:295,y:263}, {x:296,y:254}, {x:294,y:229}, {x:288,y:211}, {x:278,y:199}, {x:313,y:215}, {x:318,y:249}, {x:305,y:278}, {x:262,y:247}, {x:267,y:226}, {x:262,y:216}, {x:259,y:222}, {x:252,y:219}, {x:239,y:218}, {x:243,y:244}, {x:253,y:234}, {x:263,y:237}, {x:274,y:247}, {x:248,y:251}, {x:239,y:258}, {x:225,y:251}, {x:257,y:264}, {x:270,y:281}, {x:279,y:263}, {x:290,y:251}, {x:294,y:242}, {x:285,y:224}, {x:304,y:202}, {x:325,y:231}, {x:316,y:266}, {x:300,y:290}, {x:268,y:295}, {x:210,y:293}, {x:210,y:271}, {x:200,y:281}, {x:220,y:288}, {x:320,y:279}, {x:327,y:261}, {x:293,y:198}, {x:230,y:197}, {x:206,y:212}, {x:198,y:231}, {x:187,y:261}, {x:181,y:273}, {x:159,y:227}, {x:201,y:189}, {x:250,y:168}, {x:333,y:201}, {x:342,y:258}, {x:337,y:282}, {x:452,y:169}, {x:397,y:155}, {x:371,y:125}, {x:348,y:124}, {x:331,y:88}, {x:334,y:61}, {x:345,y:28}, {x:418,y:10}, {x:470,y:8}, {x:494,y:26}, {x:494,y:167}, {x:151,y:66}, {x:154,y:105}, {x:131,y:126}, {x:79,y:145}, {x:29,y:127}, {x:23,y:65}, {x:31,y:24}, {x:89,y:14}, {x:172,y:31}, {x:164,y:68}, {x:160,y:65}, {x:140,y:24}, {x:62,y:13}, {x:21,y:7}, {x:7,y:39}, {x:13,y:100}, {x:14,y:150}, {x:37,y:154}, {x:15,y:189}, {x:116,y:182}, {x:187,y:127}, {x:207,y:25}, {x:308,y:16}, {x:291,y:79}, {x:403,y:195}, {x:473,y:211}, {x:304,y:152}, {x:106,y:264}, {x:439,y:278}];

var data_smiley = [{x:208,y: 34}, {x:192,y: 44}, {x:174,y: 60}, {x:159,y: 92}, {x:154,y: 113}, {x:146,y: 144}, {x:155,y: 184}, {x:168,y: 211}, {x:187,y: 241}, {x:210,y: 260}, {x:242,y: 278}, {x:294,y: 284}, {x:344,y: 283}, {x:386,y: 266}, {x:396,y: 233}, {x:398,y: 200}, {x:398,y: 150}, {x:381,y: 100}, {x:359,y: 59}, {x:312,y: 36}, {x:258,y: 27}, {x:213,y: 28}, {x:184,y: 39}, {x:163,y: 58}, {x:150,y: 78}, {x:134,y: 108}, {x:123,y: 142}, {x:126,y: 175}, {x:132,y: 206}, {x:144,y: 232}, {x:175,y: 254}, {x:204,y: 274}, {x:225,y: 286}, {x:264,y: 294}, {x:285,y: 294}, {x:320,y: 289}, {x:338,y: 286}, {x:359,y: 284}, {x:375,y: 272}, {x:389,y: 252}, {x:396,y: 202}, {x:393,y: 185}, {x:392,y: 167}, {x:393,y: 135}, {x:394,y: 122}, {x:392,y: 105}, {x:387,y: 96}, {x:377,y: 83}, {x:366,y: 61}, {x:357,y: 43}, {x:340,y: 32}, {x:299,y: 24}, {x:284,y: 21}, {x:250,y: 13}, {x:235,y: 13}, {x:232,y: 18}, {x:232,y: 25}, {x:216,y: 23}, {x:203,y: 33}, {x:206,y: 48}, {x:181,y: 57}, {x:183,y: 69}, {x:166,y: 74}, {x:167,y: 81}, {x:173,y: 93}, {x:151,y: 100}, {x:142,y: 109}, {x:154,y: 132}, {x:157,y: 138}, {x:136,y: 140}, {x:131,y: 155}, {x:141,y: 167}, {x:146,y: 168}, {x:152,y: 171}, {x:139,y: 186}, {x:138,y: 194}, {x:153,y: 203}, {x:164,y: 225}, {x:163,y: 237}, {x:163,y: 247}, {x:148,y: 229}, {x:154,y: 222}, {x:171,y: 254}, {x:185,y: 263}, {x:193,y: 244}, {x:193,y: 260}, {x:219,y: 273}, {x:227,y: 267}, {x:230,y: 283}, {x:251,y: 290}, {x:272,y: 278}, {x:275,y: 290}, {x:279,y: 296}, {x:304,y: 286}, {x:312,y: 278}, {x:317,y: 298}, {x:338,y: 291}, {x:350,y: 283}, {x:359,y: 271}, {x:381,y: 271}, {x:363,y: 254}, {x:393,y: 246}, {x:383,y: 225}, {x:389,y: 223}, {x:406,y: 222}, {x:408,y: 209}, {x:401,y: 185}, {x:404,y: 175}, {x:412,y: 167}, {x:409,y: 177}, {x:406,y: 157}, {x:406,y: 197}, {x:379,y: 249}, {x:389,y: 146}, {x:407,y: 134}, {x:408,y: 125}, {x:405,y: 109}, {x:392,y: 92}, {x:384,y: 78}, {x:376,y: 55}, {x:369,y: 41}, {x:349,y: 69}, {x:336,y: 48}, {x:333,y: 35}, {x:323,y: 19}, {x:302,y: 11}, {x:277,y: 11}, {x:264,y: 14}, {x:220,y: 40}, {x:190,y: 67}, {x:242,y: 102}, {x:230,y: 115}, {x:230,y: 123}, {x:243,y: 123}, {x:250,y: 113}, {x:249,y: 108}, {x:240,y: 114}, {x:237,y: 109}, {x:315,y: 115}, {x:304,y: 122}, {x:303,y: 130}, {x:318,y: 132}, {x:329,y: 126}, {x:331,y: 120}, {x:316,y: 119}, {x:317,y: 123}, {x:326,y: 118}, {x:312,y: 126}, {x:213,y: 178}, {x:228,y: 201}, {x:244,y: 211}, {x:274,y: 220}, {x:304,y: 215}, {x:332,y: 202}, {x:340,y: 185}, {x:226,y: 185}, {x:224,y: 177}, {x:220,y: 192}, {x:230,y: 209}, {x:236,y: 197}, {x:235,y: 192}, {x:247,y: 205}, {x:248,y: 216}, {x:254,y: 217}, {x:259,y: 209}, {x:267,y: 225}, {x:278,y: 212}, {x:285,y: 211}, {x:294,y: 219}, {x:287,y: 224}, {x:307,y: 207}, {x:307,y: 202}, {x:317,y: 212}, {x:321,y: 218}, {x:323,y: 208}, {x:320,y: 204}, {x:326,y: 195}, {x:332,y: 194}, {x:331,y: 185}, {x:334,y: 176}, {x:347,y: 174}, {x:347,y: 189}, {x:136,y: 126}, {x:148,y: 123}, {x:131,y: 122}, {x:143,y: 96}, {x:149,y: 86}, {x:246,y: 28}, {x:284,y: 31}, {x:275,y: 26}, {x:301,y: 36}, {x:319,y: 22}, {x:344,y: 22}, {x:359,y: 34}, {x:387,y: 53}, {x:394,y: 78}, {x:412,y: 98}, {x:421,y: 117}, {x:426,y: 143}, {x:410,y: 149}, {x:421,y: 154}, {x:423,y: 163}, {x:415,y: 184}, {x:416,y: 195}, {x:411,y: 212}, {x:408,y: 238}, {x:403,y: 253}, {x:393,y: 269}, {x:380,y: 279}, {x:366,y: 286}, {x:329,y: 283}, {x:346,y: 273}, {x:356,y: 271}, {x:372,y: 262}, {x:356,y: 291}, {x:249,y: 298}, {x:239,y: 294}, {x:257,y: 281}, {x:215,y: 170}, {x:219,y: 170}, {x:340,y: 171}];

var data_grid = [{x:38,y:36}, {x:39,y:148}, {x:39,y:148}, {x:37,y:273}, {x:465,y:38}, {x:469,y:282}, {x:262,y:278}, {x:249,y:27}, {x:459,y:149}, {x:260,y:146}, {x:147,y:33}, {x:377,y:35}, {x:146,y:147}, {x:376,y:147}, {x:145,y:279}, {x:379,y:279}, {x:41,y:31}, {x:34,y:40}, {x:25,y:29}, {x:32,y:24}, {x:35,y:34}, {x:25,y:29}, {x:30,y:43}, {x:39,y:46}, {x:44,y:39}, {x:45,y:24}, {x:36,y:23}, {x:40,y:55}, {x:51,y:39}, {x:49,y:24}, {x:34,y:28}, {x:34,y:50}, {x:146,y:26}, {x:143,y:36}, {x:148,y:41}, {x:151,y:24}, {x:140,y:22}, {x:150,y:43}, {x:158,y:39}, {x:153,y:33}, {x:151,y:29}, {x:139,y:28}, {x:146,y:46}, {x:151,y:49}, {x:161,y:38}, {x:158,y:28}, {x:152,y:18}, {x:247,y:26}, {x:250,y:33}, {x:254,y:33}, {x:255,y:24}, {x:249,y:20}, {x:243,y:32}, {x:247,y:39}, {x:253,y:39}, {x:255,y:18}, {x:245,y:16}, {x:242,y:28}, {x:240,y:32}, {x:258,y:44}, {x:265,y:34}, {x:261,y:29}, {x:262,y:27}, {x:141,y:41}, {x:136,y:34}, {x:47,y:32}, {x:46,y:46}, {x:28,y:38}, {x:374,y:30}, {x:374,y:40}, {x:374,y:40}, {x:380,y:41}, {x:383,y:38}, {x:383,y:31}, {x:379,y:28}, {x:371,y:29}, {x:370,y:39}, {x:376,y:48}, {x:384,y:49}, {x:393,y:41}, {x:391,y:30}, {x:386,y:26}, {x:372,y:26}, {x:367,y:33}, {x:369,y:48}, {x:389,y:44}, {x:389,y:36}, {x:463,y:31}, {x:461,y:41}, {x:468,y:46}, {x:472,y:41}, {x:471,y:31}, {x:463,y:26}, {x:453,y:36}, {x:455,y:46}, {x:458,y:52}, {x:465,y:55}, {x:475,y:50}, {x:479,y:39}, {x:475,y:31}, {x:471,y:25}, {x:457,y:26}, {x:38,y:142}, {x:32,y:148}, {x:47,y:152}, {x:43,y:145}, {x:38,y:154}, {x:43,y:154}, {x:51,y:145}, {x:42,y:139}, {x:30,y:139}, {x:27,y:148}, {x:32,y:159}, {x:44,y:162}, {x:53,y:157}, {x:49,y:138}, {x:33,y:132}, {x:44,y:134}, {x:144,y:138}, {x:139,y:146}, {x:143,y:153}, {x:148,y:153}, {x:153,y:142}, {x:151,y:140}, {x:143,y:135}, {x:135,y:143}, {x:131,y:152}, {x:134,y:160}, {x:143,y:163}, {x:153,y:160}, {x:158,y:151}, {x:158,y:144}, {x:153,y:136}, {x:139,y:132}, {x:133,y:135}, {x:256,y:140}, {x:254,y:147}, {x:253,y:153}, {x:257,y:155}, {x:267,y:154}, {x:269,y:143}, {x:258,y:137}, {x:248,y:140}, {x:248,y:153}, {x:252,y:159}, {x:262,y:164}, {x:270,y:164}, {x:275,y:151}, {x:269,y:137}, {x:262,y:133}, {x:248,y:133}, {x:243,y:143}, {x:244,y:153}, {x:268,y:150}, {x:376,y:140}, {x:368,y:145}, {x:372,y:154}, {x:380,y:154}, {x:382,y:145}, {x:379,y:135}, {x:369,y:137}, {x:365,y:145}, {x:371,y:158}, {x:380,y:162}, {x:390,y:153}, {x:385,y:141}, {x:383,y:135}, {x:369,y:131}, {x:355,y:137}, {x:362,y:155}, {x:371,y:166}, {x:379,y:170}, {x:360,y:150}, {x:363,y:136}, {x:359,y:143}, {x:389,y:162}, {x:267,y:169}, {x:253,y:168}, {x:248,y:164}, {x:152,y:167}, {x:141,y:169}, {x:133,y:165}, {x:125,y:161}, {x:125,y:147}, {x:151,y:129}, {x:53,y:51}, {x:54,y:32}, {x:48,y:56}, {x:57,y:46}, {x:160,y:49}, {x:150,y:53}, {x:139,y:47}, {x:131,y:39}, {x:134,y:24}, {x:148,y:13}, {x:168,y:21}, {x:168,y:40}, {x:161,y:14}, {x:166,y:29}, {x:252,y:48}, {x:265,y:48}, {x:267,y:45}, {x:274,y:29}, {x:267,y:16}, {x:257,y:11}, {x:238,y:17}, {x:234,y:36}, {x:244,y:44}, {x:256,y:56}, {x:383,y:19}, {x:372,y:20}, {x:366,y:31}, {x:362,y:46}, {x:375,y:60}, {x:389,y:56}, {x:398,y:39}, {x:396,y:23}, {x:386,y:13}, {x:356,y:26}, {x:371,y:56}, {x:361,y:36}, {x:364,y:23}, {x:451,y:30}, {x:451,y:49}, {x:459,y:60}, {x:473,y:60}, {x:484,y:52}, {x:486,y:31}, {x:478,y:20}, {x:461,y:18}, {x:448,y:23}, {x:447,y:41}, {x:457,y:143}, {x:453,y:152}, {x:460,y:159}, {x:466,y:154}, {x:463,y:144}, {x:458,y:137}, {x:451,y:143}, {x:452,y:162}, {x:458,y:165}, {x:466,y:165}, {x:473,y:155}, {x:472,y:143}, {x:466,y:133}, {x:451,y:132}, {x:445,y:147}, {x:446,y:162}, {x:455,y:175}, {x:468,y:175}, {x:482,y:160}, {x:481,y:145}, {x:475,y:133}, {x:467,y:129}, {x:460,y:129}, {x:442,y:133}, {x:439,y:146}, {x:445,y:157}, {x:38,y:268}, {x:33,y:271}, {x:35,y:280}, {x:40,y:281}, {x:44,y:275}, {x:47,y:263}, {x:32,y:258}, {x:27,y:273}, {x:31,y:284}, {x:42,y:289}, {x:48,y:278}, {x:50,y:272}, {x:42,y:261}, {x:32,y:258}, {x:31,y:267}, {x:22,y:273}, {x:25,y:286}, {x:36,y:295}, {x:51,y:294}, {x:56,y:281}, {x:60,y:265}, {x:54,y:255}, {x:38,y:250}, {x:25,y:254}, {x:25,y:263}, {x:15,y:279}, {x:148,y:269}, {x:138,y:276}, {x:143,y:286}, {x:148,y:287}, {x:153,y:278}, {x:153,y:264}, {x:143,y:263}, {x:137,y:270}, {x:129,y:279}, {x:134,y:287}, {x:140,y:292}, {x:152,y:294}, {x:159,y:279}, {x:157,y:272}, {x:150,y:258}, {x:133,y:260}, {x:128,y:272}, {x:131,y:286}, {x:133,y:294}, {x:163,y:293}, {x:143,y:257}, {x:131,y:267}, {x:261,y:270}, {x:257,y:279}, {x:263,y:283}, {x:269,y:280}, {x:267,y:272}, {x:262,y:264}, {x:255,y:271}, {x:254,y:283}, {x:259,y:288}, {x:267,y:288}, {x:273,y:282}, {x:273,y:271}, {x:270,y:265}, {x:265,y:261}, {x:253,y:267}, {x:246,y:278}, {x:250,y:289}, {x:265,y:294}, {x:276,y:293}, {x:282,y:277}, {x:279,y:266}, {x:273,y:258}, {x:261,y:254}, {x:248,y:266}, {x:241,y:275}, {x:242,y:286}, {x:250,y:294}, {x:376,y:274}, {x:372,y:283}, {x:379,y:288}, {x:390,y:276}, {x:381,y:268}, {x:375,y:266}, {x:369,y:274}, {x:367,y:285}, {x:379,y:293}, {x:386,y:291}, {x:389,y:284}, {x:389,y:268}, {x:379,y:259}, {x:366,y:269}, {x:364,y:279}, {x:363,y:289}, {x:374,y:293}, {x:399,y:293}, {x:393,y:269}, {x:386,y:261}, {x:374,y:257}, {x:368,y:261}, {x:361,y:271}, {x:362,y:282}, {x:371,y:292}, {x:396,y:283}, {x:397,y:279}, {x:386,y:278}, {x:464,y:276}, {x:466,y:289}, {x:477,y:290}, {x:477,y:278}, {x:474,y:276}, {x:466,y:271}, {x:460,y:279}, {x:464,y:287}, {x:468,y:293}, {x:478,y:296}, {x:485,y:288}, {x:484,y:279}, {x:478,y:270}, {x:473,y:266}, {x:458,y:271}, {x:460,y:291}, {x:485,y:296}, {x:491,y:279}, {x:487,y:269}, {x:481,y:261}, {x:477,y:259}, {x:465,y:261}, {x:457,y:265}, {x:453,y:276}, {x:456,y:286}, {x:466,y:299}];

//==============================================================================
//  Functionality
//==============================================================================

// General / Drawing

function getPointClickedOn(e) {
  // cursor point in display coordinates
  var pt_client = draw.node.createSVGPoint();
  pt_client.x = e.clientX;
  pt_client.y = e.clientY;
  // cursor point, translated into svg coordinates
  var pt_svg = pt_client.matrixTransform(draw.node.getScreenCTM().inverse());
  return {x:pt_svg.x, y:pt_svg.y};
}

function addNewPoint(point) {
  if (addingDataPointsManually) {
    dataPoints.push(point);
    redrawAll();
  }
}

function drawDataPoint(pt) {
  var point;
  point = draw.circle(marker_size).attr({ cx: pt.x, cy: pt.y, fill: "#607D8B", stroke: "#607D8B" });
}


function drawEps(pt) {
  eps_drawing.back(); 
  eps_drawing.circle(rg_eps.value * 2).attr({ cx: pt.x, cy: pt.y, fill: "rgba(128, 128, 128, 0.3)" });
}

function drawBackground(myvalue) {
  if (flag == true) { 
    var tmp = 0;
    var alpha = 0;
    var pixel;
    var px=0 , py=0;
      for(py = 0; py < size_y ; py++) {
        for(px = 0; px < size_x ; px++) {
          // normalize
          if (myvalue[py][px] == 0) {
            tmp = 1;
          }
          else {
            tmp = (valuemax - myvalue[py][px]) / (valuemax - valuemin);
          }

          // set transparence
          alpha = 1 - tmp;
          
          pixel = draw.rect(1,1);
          pixel.attr('x', px).attr('y', py);
          pixel.attr({ fill: '#607D8B', 'fill-opacity': alpha });
        }
      }
  }
}

function redrawAll() {
  draw.clear();
  dataPoints.map(drawDataPoint);
}

function updateStyles() {
  if (addingDataPointsManually) {
    draw.node.classList.add("cursor-crosshair");
  } else {
    draw.node.classList.remove("cursor-crosshair");
  }
}

function epsChange() {
  eps_drawing.remove();
  eps_drawing = draw.group();
  dataPoints.map(drawEps);
}

function epsRemove() {
  setTimeout(function () {
    eps_drawing.remove();
    eps_drawing = draw.group();
  }, 1000);
}


// Data Points

function toggleAddingDataPointsManually() {
  addingDataPointsManually = !addingDataPointsManually;
  updateStyles();
}

function addDataPointsRandomly(count) {
  for (i = 0; i < count; i++) {
    var newPoint = { x: Math.random() * size_x, y: Math.random() * size_y };
    dataPoints.push(newPoint);
  }
  redrawAll();
}

function addDataPointsRandomClusters(num_clusters, cluster_size) {
  num_clusters = (typeof num_clusters !== 'undefined') ? num_clusters : 3;
  cluster_size = (typeof cluster_size !== 'undefined') ? cluster_size : 60;

  var max_x_stdev = 0.06 * size_x;
  var max_y_stdev = 0.06 * size_y;

  var cluster_centers = [];

  for (var i = 0; i < num_clusters; i++) {
    cluster_centers.push({ x: Math.random() * size_x, y: Math.random() * size_y });
  }

  cluster_centers.forEach(function (d) {
    for (var i = 0; i < cluster_size; i++) {
      var x = rnd(d.x, max_x_stdev);
      var y = rnd(d.y, max_y_stdev);
      if (x < 0 || x >= size_x) continue;
      if (y < 0 || y >= size_y) continue;
      dataPoints.push({x:x, y:y});
    }
  });

  redrawAll();
}

function addDatasetPoints(dataset) {
  dataPoints = dataPoints.concat(dataset);
  redrawAll();
}

function removeAllDataPoints() {
  dataPoints = [];
  dataPointsAssigned = {};
  redrawAll();
}


//==============================================================================
//  Kernel density estimation
//==============================================================================

function toggleKDE() {
  if (!runningKDE) {
    runKDE();
  } else {
    stopKDE();
  }
}

function runKDE() {
  // get inputs
  var eps = parseInt(rg_eps.value);
  var kfunc = sel_kfunc.value;

  // matrix of kernel density value
  var myvalue = [];
  for(var i = 0; i < size_y; i++){
    myvalue[i] = []; 
    for(var j = 0; j < size_x; j++){
      myvalue[i][j] = 0;
    }
  }

  // run KDE 
  var kderunner = jsKDE(myvalue).eps(eps).kfunc(kfunc);
  kderunner();
}

jsKDE = function jsKDE(myvalue) {

  //Local instance vars
  var eps;
  var kfunc = epanechnikovFunc;

  //Core algorithms
  function epanechnikovFunc(dataPoints, myvalue, eps) {
    h = eps;
    alert("Start timing...");
    var start = Date.now();

    var px = 0 , py = 0; // pixel point
    var pdist = 0; // distance between pixel points and dataset points

    for(py = 0 ; py < size_y ; py++) {
        for(px = 0 ; px < size_x ; px++) {
            var temp = 0;
            dataPoints.map(function (pt){
              pdist = Math.sqrt(Math.pow(px - pt.x , 2) + Math.pow(py - pt.y,2));
              
              if(pdist < h)
                temp += 2/Math.PI * (1- pdist*pdist/(h*h));
            })
      
            temp = (temp/(dataPoints.length*h*h)) * 1000000000;
            myvalue[py][px] = temp;

            // find the maximum and minimum values for normalization
            if(py==0 && px==0)
            {
                valuemin = temp;
                valuemax = temp;
            }
            else if(temp > valuemax)
                valuemax = temp;
            else if(temp < valuemin)
                valuemin = temp;
        }
    }
    
    flag = true;
   
    var end = Date.now();
    var time = end - start;
    timer.innerHTML = time/1000 + ' s';
    alert("Finished!\nAfter click Comfirm, please hold. It should take a while to paint the result.");
  
    drawBackground(myvalue);
   
  };
  
  function gaussianFunc(dataPoints, myvalue, eps) {
    h = eps;
    alert("Start timing...");
    var start = Date.now();

    var px = 0 , py = 0; // pixel point
    var pdist = 0; // distance between pixel points and dataset points

    for(py = 0 ; py < size_y ; py++) {
        for(px = 0 ; px < size_x ; px++) {
            var temp = 0;
            dataPoints.map(function (pt){
              pdist = Math.sqrt(Math.pow(px - pt.x , 2) + Math.pow(py - pt.y,2));
              
              if(pdist < h)
                temp += 1/(2*Math.PI) * Math.exp(-0.5*pdist*pdist/(h*h));
            })
    
            temp = (temp/(dataPoints.length*h*h)) * 1000000000;
            myvalue[py][px] = temp;

            // find the maximum and minimum values for normalization
            if(py==0 && px==0)
            {
                valuemin = temp;
                valuemax = temp;
            }
            else if(temp > valuemax)
                valuemax = temp;
            else if(temp < valuemin)
                valuemin = temp;
        }
    }
    
    flag = true;
   
    var end = Date.now();
    var time = end - start;
    timer.innerHTML = time/1000 + ' s';
    alert("Finished!\nAfter click Comfirm, please hold. It should take a while to paint the result.");
  
    drawBackground(myvalue);
   
  };
  
  function silvermank2Func(dataPoints, myvalue, eps) { 
    h = eps;
    alert("Start timing...");
    var start = Date.now();

    var px = 0 , py = 0; // pixel point
    var pdist = 0; // distance between pixel points and dataset points

    for(py = 0 ; py < size_y ; py++) {
        for(px = 0 ; px < size_x ; px++) {
            var temp = 0;
            dataPoints.map(function (pt){
              pdist = Math.sqrt(Math.pow(px - pt.x , 2) + Math.pow(py - pt.y,2));
              
              if(pdist < h)
                temp += 3/Math.PI * Math.pow((1- pdist*pdist/(h*h)) , 2);  
            })
      
            temp = (temp/(dataPoints.length*h*h)) * 1000000000;
            myvalue[py][px] = temp;

            // find the maximum and minimum values for normalization
            if(py==0 && px==0)
            {
                valuemin = temp;
                valuemax = temp;
            }
            else if(temp > valuemax)
                valuemax = temp;
            else if(temp < valuemin)
                valuemin = temp;
        }
    }
    
    flag = true;
   
    var end = Date.now();
    var time = end - start;
    timer.innerHTML = time/1000 + ' s';
    alert("Finished!\nAfter click Comfirm, please hold. It should take a while to paint the result.");
    
    drawBackground(myvalue);
   
  };
  
  function silvermank3Func(dataPoints, myvalue, eps) { 
    h = eps;
    alert("Start timing...");
    var start = Date.now();

    var px = 0 , py = 0; // pixel point
    var pdist = 0; // distance between pixel points and dataset points

    for(py = 0 ; py < size_y ; py++) {
        for(px = 0 ; px < size_x ; px++) {
            var temp = 0;
            dataPoints.map(function (pt){
              pdist = Math.sqrt(Math.pow(px - pt.x, 2) + Math.pow(py - pt.y, 2));
              
              if(pdist < h)
                temp += 4/Math.PI * Math.pow((1- pdist*pdist/(h*h)) , 3); 
              })
      
            temp = (temp/(dataPoints.length*h*h)) * 1000000000;  
            myvalue[py][px] = temp;
        
            // find the maximum and minimum values for normalization
            if(py==0 && px==0) {
              valuemin = temp;
              valuemax = temp;
            }
            else if(temp > valuemax)
              valuemax = temp;
            else if(temp < valuemin)
              valuemin = temp;
        }
    }
    
    flag = true;
   
    var end = Date.now();
    var time = end - start;
    timer.innerHTML = time/1000 + ' s';
    alert("Finished!\nAfter click Comfirm, please hold. It should take a while to paint the result.");
  
    drawBackground(myvalue);
   
  };
  
  var kde = function kde() {
   
    kfunc(dataPoints, myvalue, eps);
    
  };


  //Getters and setters

  kde.eps = function (e) {
    if (arguments.length === 0) {
      return eps;
    }
    if (typeof e === "number") {
      eps = e;
    }

    return kde;
  };

  kde.kfunc = function (fct) {
    if (arguments.length === 1) {
      if (typeof fct === 'string') {
        switch (fct) {
          case 'epanechnikovFunc':
            kfunc = epanechnikovFunc;
            break;
          case 'gaussianFunc':
            kfunc = gaussianFunc;
            break;
          case 'silvermank2Func':
            kfunc = silvermank2Func;
            break;
          case 'silvermank3Func':
            kfunc = silvermank3Func;
          default:
            kfunc = epanechnikovFunc;
        }
      } else if (typeof fct === 'function') {
        kfunc = fct;
      }
    }

    return kde;
  };

  return kde;
};

function stopKDE() {
  runningKDE = false;
};

//==============================================================================
//  Helper
//==============================================================================

function rnd(mean, stdev) {
  var rnd_snd = Math.random() * 2 - 1 + (Math.random() * 2 - 1) + (Math.random() * 2 - 1);
  return Math.round(rnd_snd * stdev + mean);
}

//==============================================================================
//  Initialize
//==============================================================================

addDatasetPoints(data_clusters3);

